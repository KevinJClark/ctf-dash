from flask import Flask, render_template, render_template_string, session, url_for, redirect, request
import sqlite3
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", help="Port to start the HTTP server on", default=80, action="store", dest="port")
args = parser.parse_args()
port = args.port

app = Flask(__name__)
app.secret_key = 'this_is_insecure'

conn = sqlite3.connect("ctf.db")
cursor = conn.cursor()


class User(object):
   # Defines the template for User objects
   def __init__(self, name, score, flags):
      self.name = name
      self.score = score
      self.flags = flags

   def print(self):
      print(f"User {self.name}: score [{self.score}] flags ({self.flags})")

class Flag(object):
   def __init__(self, name, value, points, location, machine, hint, hint_reduction):
      self.name = name
      self.value = value
      self.points = points
      self.location = location
      self.machine = machine
      self.hint = hint
      self.hint_reduction = hint_reduction


def setup_database(conn=conn, cursor=cursor):
   # Create the users table in the database
   # Flags achieved are in the format: 1|2|3|5
   cursor.execute("""CREATE TABLE if not exists users
                  (name text PRIMARY KEY, score integer, 
                   flags text) 
                  """)
   conn.commit()

def create_user(username, score = 0, flags = "0", conn=conn, cursor=cursor):
   # Add a new user into the database
   cursor.execute("""SELECT name FROM users WHERE name = ?""", (username,))
   rows = cursor.fetchall()
   if(len(rows) == 0):
       cursor.execute("""INSERT INTO users(name, score, flags) VALUES(?, ?, ?)""", (username, score, flags))
       conn.commit()

def capture_flag(username, points=0, flag_name="flag", conn=conn, cursor=cursor):
   # Update the status of a flag to "captured" and award the points
   # Validate the user hasn't already captured the flag before awarding points
   
   cursor.execute("""SELECT score FROM users WHERE name = ?""", (username,))
   score = cursor.fetchall()[0][0]
   cursor.execute("""SELECT flags FROM users WHERE name = ? """, (username,))
   flags = cursor.fetchall()[0][0]
   
   # Duplicate flag submitted. Do not award points and laugh at the user instead
   if(flag_name in flags.split('|')):
      print(f"[!] User {username} tried to resubmit {flag_name}!")

   # Non-duplicate flag submitted. Award the (tw: gendered languange) man his points
   else:
      flags += f"|{flag_name}"
      cursor.execute("""UPDATE users SET score = ?, flags = ? where name = ?""", (score+points, flags, username))
      conn.commit()
      print(f"[+] User {username} submitted {flag_name} for {points} points!")
   
def get_users(conn=conn, cursor=cursor):
   # returns a list[Users] object of all users in the database
   users = []
   cursor.execute("""SELECT * FROM users""")
   results = cursor.fetchall()
   for user in results:
      users.append(User(name=user[0], score=user[1], flags=user[2]))
   return users

def parse_config(config_file = "flag-config.json"):
   # Parse the ctf configuration file
   flags = []
   with open(config_file) as fd:
      config = json.load(fd)
   for flag in config['flags']:
      flags.append(Flag(name=flag['name'], value=flag['value'], points=flag['points'], location=flag['location'], machine=flag['machine'], hint=flag['hint'], hint_reduction=flag['hint_reduction'])) 
   return flags


@app.route('/register', methods=['GET', 'POST'])
def register():
   conn = sqlite3.connect("ctf.db") # We have to redefine this in this thread otherwise SQLite yells at us
   cursor = conn.cursor()
   if(request.method == 'POST'):
      # Save the form data to the session object
      session['username'] = request.form['username']
      create_user(session['username'], conn=conn, cursor=cursor) # Create a new user in the database when a user registers
      return redirect(url_for('index'))

   return """
      <!DOCTYPE html>
      <link rel="stylesheet" href="static/style.css" type="text/css" media="screen">
      <h2>
        <form method="post">
          <label for="username">Enter your username:</label>
            <input type="text" id="email" name="username" required />
            <button type="submit">Submit</button>
        </form>
      </h2>
          """

@app.route('/submit', methods=['POST'])
def submit():
   # Logic for submitting flags to the application.
   # Check for correctness before awarding points via `capture_flag()`
   conn = sqlite3.connect("ctf.db") # We have to redefine this in this thread otherwise SQLite yells at us
   cursor = conn.cursor()

   flags = parse_config()
   message = "[-] Incorrect flag!"
   form = request.form.to_dict()
   if(len(form) > 0):
      flag_name = list(form.keys())[0]
      flag_value = list(form.values())[0]
 
      for flag in flags:
         if(flag_name == flag.name and flag_value == flag.value):
            message = "[+] Correct flag!"
            capture_flag(username=session['username'], points=flag.points, flag_name=flag.name, conn=conn, cursor=cursor)

   return render_template_string("""
      <!DOCTYPE html>
      <link rel="stylesheet" href="static/style.css" type="text/css" media="screen">
      <h2>
         {{ message }} <a href="{{ url_for('index') }}"> -->Click to go back<--</a>
      </h2>
                                 """, message=message)

def custom_sort(e):
  return e.score

@app.route('/')
def index():
   # Serves the main index page - leaderboards
   conn = sqlite3.connect("ctf.db") # We have to redefine this in this thread otherwise SQLite yells at us
   cursor = conn.cursor()
   users = get_users(conn=conn, cursor=cursor)
   users.sort(reverse=True, key=custom_sort)
   flags = parse_config()

   return render_template('index.html', users=users, flags=flags)

if __name__ == '__main__':
   print(f"[*] Setting up SQLite database...")
   setup_database()
   print(f"[*] Parsing flag config file...")

   #capture_flag("dad", 20, 2)

   print(f"[*] Starting HTTP server on port {port}...\n")
   app.run(host="0.0.0.0", port=port)
